# Cartpole env modified

## Purpose
The env is modified so that the episode does not terminate when the pole falls.

- The pole can be initilize anywhere (down by default)
- The episode terminates when :
	- the pendulum does more than 5 turns
	- the cart goes after the limits of the tracks

## Reward
Three types of rewards are combined :
- Position
- Constrains
- Smoothness of the command