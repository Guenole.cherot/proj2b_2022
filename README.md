# proj2B_2022

## Simulation
Command a pendulum to make it go from the low position (stable equilibrium) to the high position (unstable equilibrium).
We will start be synthetisize a command to maintain the unstable position :
- With proportional correction on the state
- With a simple Reinforcement Learning (RL) agent

Then we will solve the complet problem by starting in the low position :
- With Model Predictive Control (MPC)
- With RL agent

## Real word experiment
Once the control will be robust in simulation, we will apply it to the control'X.