# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 10:26:56 2021

@author: Guenole

Modified version of the Classic cart-pole system implemented by Rich Sutton et al.

To complete the creating of the custom env :
    https://medium.com/@apoddar573/making-your-own-custom-environment-in-gym-c3b65ff8cdaa
    

"""
import gym
from gym import spaces, logger, core
from gym.utils import seeding


import math
from typing import Optional
import numpy as np


class CartPoleEnv(core.Env):
    """
    Description:
        A pole is attached by an un-actuated joint to a cart, which moves along
        a frictionless track. The pendulum starts upright, and the goal is to
        prevent it from falling over by increasing and reducing the cart's
        velocity.
    Source:
        This environment corresponds to the version of the cart-pole problem
        described by Barto, Sutton, and Anderson
    Observation:
        Type: Box(4)
        Num     Observation               Min                     Max
        0       Cart Position             -0.4                    0.4
        1       Cart Velocity             -Inf                    Inf
        2       Pole Angle                0                       2pi (no constrains)
        3       Pole Angular Velocity     -Inf                    Inf
    Actions:
        Type: Box
        Force to apply on the cart
        Note: The amount the velocity that is reduced or increased is not
        fixed; it depends on the angle the pole is pointing. This is because
        the center of gravity of the pole increases the amount of energy needed
        to move the cart underneath it
    Reward:
        Reward is 1 for every step taken, including the termination step
    Starting State:
        All observations are assigned a uniform random value in [-0.05..0.05]
    Episode Termination:
        Pole Angle is more than 12 degrees.
        Cart Position is more than 2.4 (center of the cart reaches the edge of
        the display).
        Episode length is greater than 200.
        Solved Requirements:
        Considered solved when the average return is greater than or equal to
        195.0 over 100 consecutive trials.
    """

    metadata = {"render.modes": ["human", "rgb_array"], "video.frames_per_second": 50}

    def __init__(self, starting_pos = "down"):
        self.gravity = 9.8
        self.masscart = 1.0
        self.masspole = 0.2 # (0.1 before modif)
        self.total_mass = self.masspole + self.masscart
        self.length = 0.1  # actually half the pole's length (0.5 before modif)
        self.polemass_length = self.masspole * self.length
        self.force_mag = 10 # (10 before modif)
        self.tau = 0.02  # seconds between state updates
        self.kinematics_integrator = "euler"
        
        ############# Added in this file #############
        self.starting_pos = starting_pos
        self.objective_state = np.asarray([0.0, 0.0, 0, 0.0])
        # Term of the cost function
        self.W = np.asarray([0, 0.4, 0.8, 0.4])
        self.coeff_x = [15,-0.35, 0.35] # 10 is the penalization, 0.5 is the length of the axis
        self.coeff_x_dot = [1,-3.5, 3.5]
        self.coeff_theta = [1,-3*np.pi, 3*np.pi] # The pole is alowed to do 3 turns before being penalized
        self.episode_length = 0
        self.max_episode_length = 400
        self.K_smooth = 0.5
        self.K_constrains = 0
        ##############################################
        
        # Angle at which to fail the episode
        self.theta_threshold_radians = 12 * 2 * math.pi / 360
        self.x_threshold = 0.5 # (2.4 before modif)

        # Angle limit set to 2 * theta_threshold_radians so failing observation
        # is still within bounds.
        
        maxi = np.array([1.0], dtype=np.float32)
        self.action_space = spaces.Box(low=-maxi, high=maxi) # action are define in percent of the power of the motor
        
        high = np.array(
            [
                self.x_threshold * 2,
                np.finfo(np.float32).max,
                self.theta_threshold_radians * 2,
                np.finfo(np.float32).max,
                1
            ],
            dtype=np.float32,
        )
        
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)

        self.seed()
        self.viewer = None
        self.state = None

        self.steps_beyond_done = None
    
    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        err_msg = f"{action!r} ({type(action)}) invalid"
        assert self.action_space.contains(action), err_msg

        x, x_dot, theta, theta_dot, action_prev = self.state
        force = self.force_mag*action[0]
        costheta = math.cos(theta)
        sintheta = math.sin(theta)

        # For the interested reader:
        # https://coneural.org/florian/papers/05_cart_pole.pdf
        temp = (
            force + self.polemass_length * theta_dot ** 2 * sintheta
        ) / self.total_mass
        thetaacc = (self.gravity * sintheta - costheta * temp) / (
            self.length * (4.0 / 3.0 - self.masspole * costheta ** 2 / self.total_mass)
        )
        xacc = temp - self.polemass_length * thetaacc * costheta / self.total_mass

        if self.kinematics_integrator == "euler":
            x = x + self.tau * x_dot
            x_dot = x_dot + self.tau * xacc
            theta = theta + self.tau * theta_dot
            theta_dot = theta_dot + self.tau * thetaacc
        else:  # semi-implicit euler
            x_dot = x_dot + self.tau * xacc
            x = x + self.tau * x_dot
            theta_dot = theta_dot + self.tau * thetaacc
            theta = theta + self.tau * theta_dot

        self.state = (x, x_dot, theta, theta_dot, action[0])
        self.episode_length += 1
        
        done = bool(
            x < -self.x_threshold
            or x > self.x_threshold
            or self.episode_length > self.max_episode_length
        )

        # Compute reward
        reward_array = self._reward_fn(self.state, action_prev)
        reward = np.sum(reward_array)
        
        # Set warning on done == True
        if not done :
            pass
        elif self.steps_beyond_done is None:
            # Pole just fell!
            self.steps_beyond_done = 0
        else:
            if self.steps_beyond_done == 0:
                logger.warn(
                    "You are calling 'step()' even though this "
                    "environment has already returned done = True. You "
                    "should always call 'reset()' once you receive 'done = "
                    "True' -- any further steps are undefined behavior."
                )
            self.steps_beyond_done += 1

        return np.array(self.state, dtype=np.float32), reward, done, {"reward_array":reward_array}
    
    def _reward_fn(self, state, action_prev):
        ret = [0,0,0]
        ret[0] = self.cost_1(state, self.objective_state, self.W)
        ret[0] += self.cost_1(state, self.objective_state, 9*self.W)
        # ret[0] += self.cost_1(state, self.objective_state, 30*self.W)
        ret[1] += self.cost_2(state[0], self.coeff_x)
        ret[1] += self.cost_2(state[1], self.coeff_x_dot)
        ret[1] += self.cost_2(state[2], self.coeff_theta)
        ret[2] += self.cost_3(action_prev, self.state[4])
        return ret
    
    def cost_1(self, state, objective_state, W):
        delta = (state[:4]-objective_state)
        return np.exp(np.sum(-0.5*delta.T*W*delta))
    
    def cost_2(self, x, coeff):
        a = coeff[0]
        b1 = coeff[1]
        b2 = coeff[2]
        if x<b1 :
            return self.K_constrains*a*(x-b1)
        elif x>b2:
            return -self.K_constrains*a*(x-b2)
        else :
            return 0
    
    def cost_3(self, action_prev, action):
        """Regularization of the command"""
        return -self.K_smooth*np.square(action_prev-action)
    
    def reset(self, seed: Optional[int] = None):
        if seed is not None or self.np_random is None:
            self.np_random, seed = seeding.np_random(seed)
            
        if self.starting_pos=="up":
            pos = 0
        elif self.starting_pos=="down":
            pos = np.pi
        elif self.starting_pos=="random":
            pos = self.np_random.uniform(-np.pi, np.pi)

        self.state = self.np_random.normal(loc=np.array([0.0, 0.0, pos, 0.0, 0.0]),
                                      scale=np.array([0.1, 0.1, 0.2, 0.2, 0.0]))
        self.steps_beyond_done = None
        self.episode_length = 0
        return np.array(self.state, dtype=np.float32)

    def render(self, mode="human"):
        screen_width = 600
        screen_height = 400

        world_width = self.x_threshold * 2
        scale = screen_width / world_width
        carty = 200  # TOP OF CART
        polewidth = 10.0
        polelen = scale * (2 * self.length)
        cartwidth = 50.0
        cartheight = 30.0

        if self.viewer is None:
            from gym.envs.classic_control import rendering

            self.viewer = rendering.Viewer(screen_width, screen_height)
            l, r, t, b = -cartwidth / 2, cartwidth / 2, cartheight / 2, -cartheight / 2
            axleoffset = cartheight / 4.0
            cart = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.carttrans = rendering.Transform()
            cart.add_attr(self.carttrans)
            self.viewer.add_geom(cart)
            l, r, t, b = (
                -polewidth / 2,
                polewidth / 2,
                polelen - polewidth / 2,
                -polewidth / 2,
            )
            pole = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            pole.set_color(0.8, 0.6, 0.4)
            self.poletrans = rendering.Transform(translation=(0, axleoffset))
            pole.add_attr(self.poletrans)
            pole.add_attr(self.carttrans)
            self.viewer.add_geom(pole)
            self.axle = rendering.make_circle(polewidth / 2)
            self.axle.add_attr(self.poletrans)
            self.axle.add_attr(self.carttrans)
            self.axle.set_color(0.5, 0.5, 0.8)
            self.viewer.add_geom(self.axle)
            self.track = rendering.Line((0, carty), (screen_width, carty))
            self.track.set_color(0, 0, 0)
            self.viewer.add_geom(self.track)

            self._pole_geom = pole

        if self.state is None:
            return None

        # Edit the pole polygon vertex
        pole = self._pole_geom
        l, r, t, b = (
            -polewidth / 2,
            polewidth / 2,
            polelen - polewidth / 2,
            -polewidth / 2,
        )
        pole.v = [(l, b), (l, t), (r, t), (r, b)]

        x = self.state
        cartx = x[0] * scale + screen_width / 2.0  # MIDDLE OF CART
        self.carttrans.set_translation(cartx, carty)
        self.poletrans.set_rotation(-x[2])

        return self.viewer.render(return_rgb_array=mode == "rgb_array")

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None