# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 14:14:13 2021

@author: Guenole
"""
import numpy as np
from stable_baselines3.common.callbacks import BaseCallback

class TensorboardCallback(BaseCallback):
    """
    Custom callback for plotting additional values in tensorboard.
    """

    def __init__(self, verbose=0):
        super(TensorboardCallback, self).__init__(verbose)
        self.reward_mean = np.zeros(3)
        self.n_steps = 0
    
    def _on_step(self) -> bool:
        self.reward_mean += self.training_env.buf_infos[0]["reward_array"]
        self.n_steps += 1
        if self.training_env.buf_dones[0] :
            self.reward_mean /= self.n_steps
            self.logger.record("Reward detail/State objective", self.reward_mean[0])
            self.logger.record("Reward detail/Constrains", self.reward_mean[1])
            self.logger.record("Reward detail/Command Smoothness", self.reward_mean[2])
            self.reward_mean = np.zeros(3)
            self.n_steps = 0
        return True