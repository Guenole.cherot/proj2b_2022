#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 12:56:02 2021

@author: cherot
"""
import gym
import numpy as np
from gym import spaces

class HistoryWrapper(gym.Wrapper):
    """
    Track history of observations for given amount of steps
    Initial steps are zero-filled
    """
    def __init__(self, env, steps):
        super().__init__(env)
        observation_space = self.env.observation_space
        high = np.tile(observation_space.high, (steps,1))
        low = np.tile(observation_space.low, (steps,1))
        self.observation_space = spaces.Box(low, high, dtype=np.float32)
        self.steps = steps
        self.history = self.make_history()

    def make_history(self):
        return [np.zeros(shape=self.env.observation_space.shape) for _ in range(self.steps)]

    def step(self, action):
        obs, reward, done, info = self.env.step(action)
        self.history.pop(0)
        self.history.append(obs)
        return np.array(self.history), reward, done, info

    def reset(self):
        self.env.reset()
        self.history = self.make_history()
        self.history.pop(0)
        self.history.append(self.env.reset())
        return np.array(self.history)