# -*- coding: utf-8 -*-
"""
Created 11/09/2021

@author: Guenole CHEROT
"""
from project.agent import Agent_K_state, Agent_constant, Agent_random, Agent_proportionnal
import gym
import gym_cartpole
import os
from project.history_wrapper import HistoryWrapper

os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"


def test_model(typ = "random", starting_pos="up"):
    env = gym.make('cartpole-v0')
    env.starting_pos = starting_pos
    if typ == "random" :
        model = Agent_random(env)
    if typ == "Proportionnal" :
        model = Agent_proportionnal(env)
    if typ == "k" :
        model = Agent_K_state(env)
    if typ == "constant":
        model = Agent_constant(env, 0)
    model.simulate(env, False)

if __name__ == "__main__":
    # test_model(typ = "constant", starting_pos="up")
    # test_model(typ = "constant", starting_pos="down")
    # test_model(typ = "random", starting_pos="up")
    # test_model(typ = "random", starting_pos="down")
    # test_model(typ = "Proportionnal", starting_pos="up")
    # test_model(typ = "Proportionnal", starting_pos="down")
    test_model(typ = "k", starting_pos="up")
    # test_model(typ = "k", starting_pos="down")
    pass