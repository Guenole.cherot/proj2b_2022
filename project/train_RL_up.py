# -*- coding: utf-8 -*-
"""
Created on Sun Dec 19 13:41:09 2021

@author: Guenole

To activate tensorboard, then open http://localhost:6006/ in browser
cd Documents\cours\Proj2B\proj2b_2022\TensorBoard
tensorboard --logdir ./
"""

from stable_baselines3 import SAC # PPO is fast but not sample efficient, SAC is supposed to be the best
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"

import gym
import gym_cartpole
import time

# Custom functions
from project.custom_callback import TensorboardCallback
from stable_baselines3.common.callbacks import CheckpointCallback
from project.plots import plot_performance_during_training
from project.history_wrapper import HistoryWrapper

def train_RL(name, env, total_timesteps=350000):
    model = SAC("MlpPolicy", env, verbose=0, tensorboard_log="TensorBoard/", device="cpu")
    checkpoint_callback = CheckpointCallback(save_freq=5000, save_path='logs/{0}'.format(name), name_prefix=name) # Save a checkpoint every 10000 steps
    model.learn(total_timesteps=total_timesteps, log_interval=4, tb_log_name = name, callback=[TensorboardCallback(), checkpoint_callback])

def init_env(starting_pos="up", n_history=1):
    env = gym.make('cartpole-v0')
    env.starting_pos = starting_pos
    env = HistoryWrapper(env, n_history)
    return env

if __name__ == "__main__":
    start = ["down"] # ["up", "random", "down"]
    total_timesteps = 25000
    for starting_pos in start :
        for k in range(10):
            name = starting_pos + "_" + str(k)
            env = init_env(starting_pos)
            train_RL(name, env, total_timesteps=total_timesteps)
            plot_performance_during_training(name, env, 5)