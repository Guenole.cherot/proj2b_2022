# -*- coding: utf-8 -*-
"""
Created 11/09/2021

@author: Guenole CHEROT
"""
import numpy as np
import matplotlib.pyplot as plt
import control
import time




class Agent():
    """Takes decision : what velocity to apply on the cart."""

    def __init__(self, env):
        self.env = env
        
    def train(self, env, n_step):
        """Train the agent on a particular env.
        """
        raise ValueError("train method not implemented") # Not implemented (astract method, not all agent are able to learn from experience)
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        raise ValueError("act method not implemented") # (Abstract method)
        
    def simulate(self, env, slow=False):
        obs = env.reset()
        done = False
        rew = []
        act = []
        while not done:
            action = self.predict(obs)
            obs, reward, done, info = env.step(action)
            rew.append(reward)
            act.append(action)
            env.render()
            if slow :
                time.sleep(0.2)
        env.close()
        # Plot
        fig, ax1 = plt.subplots()
        color = 'tab:red'
        ax1.set_xlabel('time')
        ax1.set_ylabel('reward', color=color)
        ax1.plot(rew, color=color)
        ax1.tick_params(axis='y', labelcolor=color)
        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:blue'
        ax2.set_ylabel('action', color=color)  # we already handled the x-label with ax1
        ax2.plot(act, color=color)
        ax2.tick_params(axis='y', labelcolor=color)
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.title("Reward and action throughout the simulation")
        plt.show()
        
                
class Agent_random(Agent):
    """Apply the simple strategy : state control."""
    def __init__(self, env):
        """"""
        super(Agent_random, self).__init__(env)
        self.action_space = env.action_space
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        return self.action_space.sample()

class Agent_proportionnal(Agent):
    """Apply the simple strategy : state control."""
    def __init__(self, env):
        """"""
        super(Agent_proportionnal, self).__init__(env)
        self.action_space = env.action_space
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        return np.clip(-0.3*obs[0], self.action_space.low, self.action_space.high)

class Agent_constant(Agent):
    """Apply the simple strategy : state control."""
    def __init__(self, env, u):
        """"""
        super(Agent_constant, self).__init__(env)
        self.u = u
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        return [self.u]
    
class Agent_K_state(Agent):
    """Apply the simple strategy : state control."""
    def __init__(self, env):
        """"""
        super(Agent_K_state, self).__init__(env)
        self.action_space = env.action_space
        # Place the poles
        P = [-2,-1.5,-1,-0.5] # Target pole
        g = self.env.gravity
        m = self.env.masspole
        M = self.env.masscart
        L = self.env.length
        A = [[0,1,0,0],
             [0,0,g*m/M,0],
             [0,0,0,1],
             [0,0,(g*(M+m))/(M*L),0],]
        B = [[0], [1/M], [0], [1/(M*L)]]
        self.K = np.asarray(control.place(A, B, P))
        
        # Parameter for the external loop
        self.epsilon = 0
        self.Kp = 0.5
    
    def predict(self, obs, *args, **kwargs):
        """Finds the best action action to do given an observation."""
        # State control
        u = np.clip(np.sum(self.K*obs[:4]),self.action_space.low,self.action_space.high) # stable equilibrium
        # PI external loop
        self.epsilon = obs[0]/self.env.x_threshold
        u += self.Kp * self.epsilon
        
        return np.clip(u,self.action_space.low,self.action_space.high)
    
