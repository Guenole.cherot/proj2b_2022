# -*- coding: utf-8 -*-
import time
import matplotlib.pyplot as plt

import os
from os import listdir
from os.path import isfile, join
import numpy as np

import gym
from stable_baselines3 import SAC

def plot_results(model, env, max_traj=5):
    obs = env.reset()
    act = []
    rew = []
    n_traj = 0
    while n_traj < max_traj:
        action, _states = model.predict(obs, deterministic=True)
        act.append(action)
        obs, reward, done, info = env.step(action)
        rew.append(reward)
        env.render()
        time.sleep(0)
        if done:
            n_traj += 1
            obs = env.reset()
            plot_graph(rew, act)
            act = []
            rew = []
    env.close()

def plot_graph(rew, act):
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_xlabel('time')
    ax1.set_ylabel('reward', color=color)
    ax1.plot(rew, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    ax2.set_ylabel('action', color=color)  # we already handled the x-label with ax1
    ax2.plot(act, color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()

def plot_performance_during_training(name, env,max_traj=1):
    abs_path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(abs_path, "../logs", name)
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    number = []
    for file in onlyfiles :
        number.append(float(file.split("_")[2]))
    number = np.asarray(number)
    number = np.sort(number)
    number = number.astype(int)
    for n in number :
        file = name + "_" + str(n) + "_steps"
        path = os.path.join(abs_path, "../logs", name) 
        model = SAC.load(os.path.join(path, file))
        print(n, "\n")
        plot_results(model, env=env, max_traj=max_traj)